#ifndef _SERIALIZATION_
#define _SERIALIZATION_

#include <string>

class Serialization {
public:
    static std::string getStringToChar(char endChar, unsigned int *startIndex, const std::string line);

};

#endif
