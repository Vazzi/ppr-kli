#include <iostream>
#include <stdio.h>
#include "mpi.h"
#include "graph.h"
#include "cliqueEvaluation.h"
#include <string.h>
#include <string>
#include "constants.h"
#include "communication.h"

// Odkomentovat pokud je potřeba debug 
// #define DEBUG 1 

using namespace std;

bool simulateWork(int myRank, Communication *com) {
	for (int i = 0; i < (myRank + 10) * (myRank + 2); i++) {
		if (i % 2 == 0) {
			int msg = com->didIRecievedSomething();
			if (MSG_FINISH == msg) {
                return true;
            }
		}
	}
	return false;
}

int loadGraph(Graph *graph, const char *inputPath) {
    cout << "Loading graph..." << endl;

    try {
        graph->loadFromFile(inputPath);
    } catch (int e) {
        cout << "File loading error." << endl;
        return 1;
    }
    cout << "Graph loaded." << endl;

    return 0;
}

void sortGraph(Graph *graph) {
    cout << "Sorting graph..." << endl;
    graph->sortVerticesByDegree();
    cout << "Graph sorted." << endl;
} 

void printGraphInfo(Graph *graph, CliqueEvaluation *eval) {
    cout << *graph;
    cout << "Max possible clique size: " << eval->getMaxPossibleCliqueSize() << endl;
}

int main(int argc, char* argv[]) {
    int myRank;
    int p;
    double t1, t2;

	string serializedMsg = "";

    MPI_Init( &argc, &argv );
    // Find out process rank
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    // Find out number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &p);
	
    Communication com = Communication(myRank,p);
	
    CliqueEvaluation eval = CliqueEvaluation(&com);
    Graph *graph = NULL;

    t1=MPI_Wtime();

    if (myRank != 0) {
		serializedMsg = com.recvGraph();
		graph = eval.unserialize(serializedMsg);
    } else {
        printf("There are %d processes.\n", p);
        graph = new Graph();
        // Load graph
        char *inputPath = argv[1];
        if (loadGraph(graph, inputPath) == 1) {
            return 1;
        }
        sortGraph(graph);
        eval.addGraph(graph);
        serializedMsg = eval.serialize();
		com.sendGraph(serializedMsg);
        cout << "Computing started..." << endl;
    }

    eval.initStackWithVerticesByPRank(myRank, p);
    eval.findMaxClique();
#if DEBUG
   	cout << myRank << ": Clique: " << eval.foundedCliqueSize() << endl;
#endif
	if (myRank == 0) {
		cout << "Clique: " << eval.foundedCliqueSize() << endl;
	}
    delete graph;
#if DEBUG
    printf("Proccessor %d ended.\n", myRank);
#endif
    t2=MPI_Wtime();

    printf("%d: Elapsed time is %f.\n", myRank, t2 - t1);

    MPI_Finalize();

    return 0;
}


