#include "cliqueEvaluation.h"

#include "vertex.h"
#include "serialization.h"
#include "constants.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <exception>

using namespace std;

CliqueEvaluation::CliqueEvaluation(Communication *communication) {
    this->stack = VertexStack();
    this->maxFoundedCliqueSize = 0;
    this->maxPossibleCliqueSize = 0;
    this->communication = communication;
}

void CliqueEvaluation::addGraph(Graph *graph) {
    this->graph = graph;
}

CliqueEvaluation::~CliqueEvaluation() {
    this->graph = NULL;
    this->communication = NULL;
}

//---------------PUBLIC-------------------

int CliqueEvaluation::foundedCliqueSize() {
    return this->maxFoundedCliqueSize;
}

string CliqueEvaluation::serialize() {
    if (this->graph == NULL) {
        return "";
    }
    stringstream ss;
    ss << GRAPH_DELIMITER << this->graph->serialize() << GRAPH_DELIMITER;
    ss << this->getMaxPossibleCliqueSize() << GRAPH_DELIMITER;
    return ss.str();
}

Graph *CliqueEvaluation::unserialize(const string line) {
    unsigned int i = 1; // First is delimiter
    string graphLine = Serialization::getStringToChar(GRAPH_DELIMITER, &i, line);
    this->graph = new Graph();
    this->graph->unserialize(graphLine);
    i++;
    string value = Serialization::getStringToChar(GRAPH_DELIMITER, &i, line);
    this->maxPossibleCliqueSize = atoi(value.c_str());
    return this->graph;
}

int CliqueEvaluation::getMaxPossibleCliqueSize() {
   if (this->maxPossibleCliqueSize == 0) {
        this->computeMaxPossibleCliqueSize();
   }
   return this->maxPossibleCliqueSize;
}

void CliqueEvaluation::computeMaxPossibleCliqueSize() {
    int maxSize = this->graph->getVertex(0)->degree + 1;
    int verticesCount = 0;

    for (int i = 0; i < this->graph->size(); i++) {
        ++verticesCount;
        Vertex *vertex = this->graph->getVertexByPosition(i);
        if (vertex->degree < maxSize) {
            maxSize = vertex->degree + 1;
        }
        if (verticesCount >= maxSize) {
            break;
        }
    }

    this->maxPossibleCliqueSize = maxSize;
}

void CliqueEvaluation::initStackWithVerticesByPRank(const int rank, const int p) {
    for (int i = rank; i < this->graph->size(); i += p) {
        Vertex *vertex = this->graph->getVertexByPosition(i);

        // Create stack item
        VertexStackItem *stackItem = new VertexStackItem(vertex, this->graph->size());
        stackItem->clique.addVertex(vertex);
        this->stack.push(stackItem);
    }
}

void CliqueEvaluation::findMaxClique() {
    bool isSomethingToEvaluate = true;
		while (isSomethingToEvaluate) {
		    int step = 0;
		    while (!this->stack.isEmpty()) {
		        if (step++ >= STEPS_COUNT_TO_RECV) {
		            step = 0;
		            if (this->handleRecevedMessages()) {
						this->clearStack();
         				return;
					}
		        }

		        VertexStackItem *nextVertex = this->stack.top();
		        this->newMaxFoundedCliqueSize(nextVertex->clique.size());
		        if (this->maxFoundedCliqueSize >= this->maxPossibleCliqueSize) {
		            this->cliqueFoundedEndProccess();
		            break;
		        }
		        
		        if (!this->checkNeighboursOf(nextVertex)) {
		            this->stack.pop();
		        }    
		    }
		    // Ask for work
		    string recvStack = this->communication->askForWork(this->maxFoundedCliqueSize);
		    if (recvStack == "") {
				if (this->foundedCliqueSize() < this->communication->foundedCliqueSize) {
					this->maxFoundedCliqueSize = this->communication->foundedCliqueSize;
				}
                this->clearStack();
                return;
		    } else {
		        this->unserializeStack(recvStack);
		    }
		}
  
}


//---------------PRIVATE-------------------
bool CliqueEvaluation::checkNeighboursOf(VertexStackItem *stackItem) {
    for (int i = stackItem->neighboursChecked; i < stackItem->vertex->degree; i++) {
        int neighbourIndex = stackItem->vertex->neighboursIndexes[i];

        if (stackItem->checkedIndexes[neighbourIndex] == true) {
            continue;
        }

        stackItem->checkedIndexes[neighbourIndex] = true;

        Vertex *neighbour = this->graph->getVertex(neighbourIndex);

        if (neighbour->degree < stackItem->clique.size()) {
            continue;
        }

        if (stackItem->clique.isInClique(neighbour)) {
            continue;
        }

        // Neighbour vertex belong to clique?
        if (stackItem->clique.doVertexBelongToClique(neighbour)) {
            // Update next neighbour index to check 
            stackItem->neighboursChecked = i + 1;
            // Add neighbour vertex to stack
            this->stack.push(this->createVertexStackItem(neighbour, stackItem)); 
            return true;
        }
    }

    return false;
}

bool CliqueEvaluation::handleRecevedMessages() {
    int msgType = this->communication->didIRecievedSomething();
    if (msgType == MSG_WORK_REQUEST) {
        this->sendHalfStackToProccesser();
    } else if (msgType == MSG_FINISH) {
        if (this->foundedCliqueSize() < this->communication->foundedCliqueSize) {
            this->maxFoundedCliqueSize = this->communication->foundedCliqueSize;
        }
        return true;
    } else if (msgType == MSG_FINISH_CLIQUE) {
        this->communication->sendCliqueSize(this->maxFoundedCliqueSize);
	}
	return false;
}

void CliqueEvaluation::sendHalfStackToProccesser() {
    if (this->stack.size() <= 1) {
        this->communication->sendNoWork();
        return;
    }
    string serializedStack = this->serializeHalfStack();
    this->communication->sendWork(serializedStack);
}

void CliqueEvaluation::unserializeStack(const string line) {
    unsigned int i = 1; // First is delimiter
    while(i < line.length()) {
        string serializedItem = Serialization::getStringToChar(STACK_DELIMITER, &i, line);
        VertexStackItem *stackItem = new VertexStackItem(serializedItem, this->graph);
        this->stack.push(stackItem);
        i++;i++;
    } 
}

string CliqueEvaluation::serializeHalfStack() {
    int verticesToSendCount = this->stack.size() / 2;
    string serialized = "" ;
    for (int i = verticesToSendCount; i > 0; i--) {
        VertexStackItem *item = this->stack.top();
		string addStackItem = STACK_DELIMITER + item->serialize() + STACK_DELIMITER;
		if ((serialized.length() + addStackItem.length()) > MESSAGE_LENGTH ) {
			break;
		}
        serialized += addStackItem;
        this->stack.pop();
    }
    return serialized;
}

void CliqueEvaluation::cliqueFoundedEndProccess() {
    cout << "Max " << this->maxPossibleCliqueSize << endl;
    cout << "Founded clique size: " << this->maxFoundedCliqueSize << endl;
    this->communication->sendCliqueFounded(this->maxFoundedCliqueSize);
    this->clearStack();
}

void CliqueEvaluation::clearStack() {
    while(!this->stack.isEmpty()) {
        this->stack.pop();
    }
}

void CliqueEvaluation::newMaxFoundedCliqueSize(int const size) {
    if (size > this->maxFoundedCliqueSize) {
        this->maxFoundedCliqueSize = size;
    }
}

VertexStackItem *CliqueEvaluation::createVertexStackItem(Vertex *vertex, VertexStackItem * fromStackItem) {
    VertexStackItem *newStackItem = new VertexStackItem(vertex, this->graph->size());
    newStackItem->setCheckedIndexes(fromStackItem->checkedIndexes);
    newStackItem->clique = fromStackItem->clique;
    newStackItem->clique.addVertex(vertex);

    return newStackItem;
}
