#ifndef _COMMUNICATION_
#define _COMMUNICATION_

#include <string>


class Communication {
    
public:
    Communication(const int myIndex,const int procCount);
    ~Communication();

    void sendGraph(std::string serializedMsg);
    std::string recvGraph();

    void sendCliqueFounded(int cliqueSize);
	void recvCliqueFounded();
    int didIRecievedSomething();

    void sendWork(std::string serializedStack);
    void sendNoWork();
    std::string recvNoWork();
	std::string recvWork();
	std::string askForWork(int maxFoundedCliqueSize);
	
	void sendFinishAll();
	void sendCliqueSize(int cliqueSize);
	void finish();
	void sendToken();
	bool recvToken(int from);
	friend std::ostream & operator<< (std::ostream &output, const Communication &com);
	
	int foundedCliqueSize;
    
private:
    int lastRecvProccesserIndex; 
    int myIndex;
	int procCount;
	int requestDest;
	int color;
	int token;
	char *message;   
	bool waitForWork;
};

#endif
