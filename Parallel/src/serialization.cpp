#include "serialization.h"

using namespace std;

string Serialization::getStringToChar(char endChar, unsigned int *startIndex, const string line) {
    string str = "";
    while (line[*startIndex] != endChar) {
        str += line[*startIndex];
        (*startIndex)++;
    }
    return str;
}
