#include "vertex.h"

#include <algorithm>
#include <sstream>
#include "constants.h"
using namespace std;

Vertex::Vertex(const int index, const string matrixLine) {
    this->index = index;
    this->degree = 0;
    this->matrixLine = matrixLine;

    this->setupNeighboursAndDegree(matrixLine);
}

Vertex::~Vertex() {
    // Empty
}

//---------------PUBLIC-------------------
bool Vertex::isNeighbour(const int index) const{
    if (this->matrixLine[index] == '0') {
        return false;
    }
	return true;
}

bool Vertex::operator< (const Vertex &vertex) const{
    return degree < vertex.degree;
}

ostream & operator<< ( ostream &output, const Vertex &vertex) {
    
    output << vertex.index << ": ";
    output << "Degree: " << vertex.degree << "; ";
    output << "Neighbours:";

    for (unsigned int i = 0; i < vertex.neighboursIndexes.size(); i++ ) {
        output << " " << vertex.neighboursIndexes[i];
    }

    output << ";";

    return output;
}

string Vertex::serialize() {
	string ret = "";
	stringstream ss;
	ss << this->index;

	ret = ss.str() + VERTEX_INCIDENCE_DELIMITER + this->matrixLine;
	return ret;
}

//---------------PROTECTED-------------------
void Vertex::setupNeighboursAndDegree(const string matrixLine) {
    for (unsigned int i = 0; i < matrixLine.size(); i++) {
        if (matrixLine[i] == '1') {
            // Add degree
            this->degree++;
            // Add neighbour
            this->neighboursIndexes.push_back(i);
        }
    }
    
}

