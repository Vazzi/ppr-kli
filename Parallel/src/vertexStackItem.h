#ifndef _VERTEX_STACK_ITEM_
#define _VERTEX_STACK_ITEM_

#include "vertex.h"
#include "clique.h"
#include "graph.h"
#include <string>

class VertexStackItem {

public:
    int neighboursChecked;
    bool *checkedIndexes;
    Clique clique;
    Vertex *vertex;

    VertexStackItem(const std::string line, Graph *graph);
    VertexStackItem(Vertex *vertex, const int verticesCount);
    ~VertexStackItem();

    void setCheckedIndexes(const bool *copyCheckedIndexes); 
    std::string serialize() const;
    
private:
    int verticesCount;

    void initCheckedIndexes();
    void unserializeVertex(const std::string line, Graph *graph);
    void unserializeCheckedIndexes(const std::string line, const int size);
    void unserializeClique(const std::string line, Graph *graph);
};

#endif
