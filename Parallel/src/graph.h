#ifndef _GRAPH_
#define _GRAPH_

#include <vector>
#include <ostream>
#include <string>
#include "vertex.h"

class Graph {

public:
    Graph();
    ~Graph();
    void loadFromFile(const char *path);
    void sortVerticesByDegree();
    int size() const;
    Vertex *getVertex(const int index);
    Vertex *getVertexByPosition(const int index);
    std::vector<Vertex *> getVertices();
	std::string serialize();
	void unserialize(std::string line);
    friend std::ostream & operator<< (std::ostream &output, const Graph &graph);

protected:
    std::vector<Vertex> vertices;
    int verticesCount;
    int * vertexIndexes;

};

#endif
