#ifndef _VERTEX_
#define _VERTEX_

#include <string>
#include <vector>
#include <ostream>

class Vertex {

public:
    int degree;
    int index;
    std::vector<int> neighboursIndexes;
    std::string matrixLine;

    Vertex(const int index, const std::string matrixLine);
    ~Vertex();
    bool isNeighbour(const int index) const;
	std::string serialize();
    bool operator< (const Vertex &vertex) const;
    friend std::ostream & operator<< (std::ostream &output, const Vertex &vertex);

protected:
    void setupNeighboursAndDegree(const std::string matrixLine);

};

#endif
