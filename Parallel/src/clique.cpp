#include "clique.h"

#include <sstream>
#include "constants.h"

using namespace std;

Clique::Clique() {
    this->vertices = vector<Vertex *>();
}

Clique::Clique(Clique *clique) {
    this->vertices = clique->vertices; 
}

Clique::~Clique() {
    this->vertices.clear();
}

//---------------PUBLIC-------------------

int Clique::size() const {
    return this->vertices.size();
}

void Clique::addVertex(Vertex *newVertex) {
    this->vertices.push_back(newVertex);
}

Vertex* Clique::getVertex(const int index) const {
    // If index is out of bound return NULL
    if (index < 0 || index >= this->size()) {
        return NULL;
    }
    return this->vertices[index];
}

bool Clique::isInClique(const Vertex *vertex) const {
    for (unsigned int i = 0; i < this->vertices.size(); i++) {
        if (this->vertices[i] == vertex) {
            return true;
        }
    }
    return false;
}

bool Clique::doVertexBelongToClique(Vertex *vertex) {
    for (int i = 0; i < this->size(); i++) {
        if (!this->vertices[i]->isNeighbour(vertex->index)) {
            return false;
        }
    }
    return true;
}

void Clique::removeVertex(Vertex *vertex) {
    for (int i = this->size() - 1; i >= 0; i--) {
        if (this->vertices[i] == vertex) {
            vertices.erase(vertices.begin() + i);
        }
    }
}

Clique & Clique::operator= (const Clique &clique) {
    if (this != &clique) {
        for (int i = 0; i < clique.size(); i++) {
            this->addVertex(clique.getVertex(i));
        }
    }
    return *this;
}

ostream & operator<< (ostream &output, const Clique &clique) {
    for (unsigned int i = 0; i < clique.vertices.size(); i++) {
        output << clique.vertices[i]->index << " ";
    }
    output << endl;

    return output;
}

string Clique::serialize() const{
    stringstream ss;
    ss << CLIQUE_DELIMITER;
    for (int i = 0; i < this->size(); i++) {
        ss << this->vertices[i]->index << CLIQUE_DELIMITER;
    }
	
    return ss.str();
}
