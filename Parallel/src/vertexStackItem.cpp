#include "vertexStackItem.h"
#include <sstream>
#include "constants.h"
#include "serialization.h"
#include <algorithm>

using namespace std;

VertexStackItem::VertexStackItem(const string line, Graph *graph) {
    unsigned int i = 0;
    this->verticesCount = graph->size();
    // First is delimiter
    i++;
    // Get vertex
    string str = Serialization::getStringToChar(VSITEM_DELIMITER, &i, line);
    this->unserializeVertex(str, graph);
    i++;
    // Get numberOfVertices
    str = Serialization::getStringToChar(VSITEM_DELIMITER, &i, line);
    this->neighboursChecked = atoi(str.c_str());
    i++;
    // Get checkedIndexes 
    str = Serialization::getStringToChar(VSITEM_DELIMITER, &i, line);
    this->unserializeCheckedIndexes(str, this->verticesCount);
    i++;
    str = Serialization::getStringToChar(VSITEM_DELIMITER, &i, line);
    this->unserializeClique(str, graph);
}

void VertexStackItem::unserializeVertex(const string indexStr, Graph *graph) {
    int index = atoi(indexStr.c_str());
    this->vertex = graph->getVertex(index);
}

void VertexStackItem::unserializeCheckedIndexes(const string line, const int size) {
    this->checkedIndexes = new bool[this->verticesCount];
    for (unsigned int i = 0; i < line.length(); i++) {
        this->checkedIndexes[i] = (line[i] != '1') ? true : false;
    }
}

void VertexStackItem::unserializeClique(const string line, Graph *graph) {
    this->clique = Clique();
    unsigned int i = 1; // First is delimiter

    string vertexStr;
    while(i < line.length()) {
        vertexStr = Serialization::getStringToChar(CLIQUE_DELIMITER, &i, line);
        int index = atoi(vertexStr.c_str());
        this->clique.addVertex(graph->getVertex(index));
        i++;
    }

}

VertexStackItem::VertexStackItem(Vertex *vertex, const int verticesCount) {
    this->vertex = vertex;
    this->clique = Clique();
    this->neighboursChecked = 0;
    this->verticesCount = verticesCount;
    this->initCheckedIndexes();
}

VertexStackItem::~VertexStackItem() {
    this->vertex = NULL;
    delete this->checkedIndexes;
}

void VertexStackItem::initCheckedIndexes() {
    this->checkedIndexes = new bool[verticesCount];

    for (int i = 0; i < this->verticesCount; i++) {
        this->checkedIndexes[i] = false;
    } 
}

void VertexStackItem::setCheckedIndexes(const bool *copyCheckedIndexes) {
    for (int i = 0; i < this->verticesCount; i++) {
        this->checkedIndexes[i] = copyCheckedIndexes[i];
    }
}

string VertexStackItem::serialize() const{
    // VertexIndex + neighboursChecked + CheckedIndexes + Clique
    stringstream ss;
    ss << VSITEM_DELIMITER << this->vertex->index << VSITEM_DELIMITER << this->neighboursChecked << VSITEM_DELIMITER;
    for (int i = 0; i < this->verticesCount; i++) {
        ss << (this->checkedIndexes ? "1" : "0");
    }
    ss << VSITEM_DELIMITER;
    ss << clique.serialize() << VSITEM_DELIMITER;

    return ss.str();
    
}
