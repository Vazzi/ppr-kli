#include "communication.h"
#include "constants.h" 
#include "mpi.h"
#include <iostream>

using namespace std;

//#define DEBUG 1

Communication::Communication(const int myIndex, const int procCount) {
    this->myIndex = myIndex;
    this->lastRecvProccesserIndex = myIndex;
	this->requestDest = (myIndex + 1) % procCount;
	this->color = WHITE;
	this->message = new char[MESSAGE_LENGTH];
	this->token = NONE;
	if (myIndex == 0) {
		this->token = WHITE;
	}
	this->foundedCliqueSize = 0;
	this->procCount = procCount;
	this->waitForWork = false;
}

Communication::~Communication() {
    delete[] this->message;
}

ostream & operator<< (ostream &output, const Communication &com) {
    output << "Index: " << com.myIndex << " token: " << com.token << " color: " << com.color << " procCount: " << com.procCount;
	output << " source: " << com.lastRecvProccesserIndex << " dest: " << com.requestDest << endl;
    return output; 
}

void Communication::sendGraph(string serializedMsg) {
	int length = serializedMsg.length() + 1;
    char *msg = strdup(serializedMsg.c_str());
    for (int source = 1; source < this->procCount; source++) {
	    MPI_Send(msg, length, MPI_CHAR, source, MSG_GRAPH, MPI_COMM_WORLD);
	}
}

string Communication::recvGraph() {
	MPI_Status status;
    MPI_Recv(this->message, MESSAGE_LENGTH, MPI_CHAR, MPI_ANY_SOURCE, MSG_GRAPH, MPI_COMM_WORLD, &status);
	return string(this->message);
}

void Communication::sendCliqueFounded(int cliqueSize) {
	if (this->myIndex != 0) {
		MPI_Send (&cliqueSize, 1, MPI_INT, 0, MSG_FOUNDED_CLIQUE, MPI_COMM_WORLD);	
	} else {
		this->sendFinishAll();
	}
}

void Communication::recvCliqueFounded() {
	MPI_Status status;
    MPI_Recv(&this->foundedCliqueSize, 1, MPI_INT, MPI_ANY_SOURCE, MSG_FOUNDED_CLIQUE, MPI_COMM_WORLD, &status);
	this->sendFinishAll();
}

int Communication::didIRecievedSomething() {
	int flag;
	MPI_Status status;
	int i;
	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
    if (flag){
  		switch (status.MPI_TAG) {
			case MSG_FOUNDED_CLIQUE:
				this->recvCliqueFounded();
				return MSG_FINISH;
			case MSG_FINISH_CLIQUE:
				MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, MSG_FINISH_CLIQUE, MPI_COMM_WORLD, &status);
				return MSG_FINISH_CLIQUE;
			case MSG_GRAPH:
				this->recvGraph();			
				return MSG_GRAPH;
			case MSG_TOKEN:
				if(recvToken(status.MPI_SOURCE))return MSG_FINISH;
				return MSG_TOKEN;
			case MSG_FINISH:
				return MSG_FINISH;
			case MSG_WORK_REQUEST:
						MPI_Recv(&i, 1, MPI_INT, status.MPI_SOURCE, MSG_WORK_REQUEST, MPI_COMM_WORLD, &status);
						this->lastRecvProccesserIndex = status.MPI_SOURCE;
				return MSG_WORK_REQUEST;
			case MSG_WORK_SENT:
				this->recvWork();
				return MSG_WORK_SENT;
			case MSG_WORK_NOWORK:
				this->recvNoWork();
                return MSG_WORK_NOWORK;
		}
	}

    return 0;
}

void Communication::sendFinishAll() {
	for (int dest = 1; dest < this->procCount; dest++) {
		MPI_Send (&this->color, 1, MPI_INT, dest, MSG_FINISH, MPI_COMM_WORLD);
	}
}

void Communication::sendCliqueSize(int cliqueSize) {
	MPI_Send (&cliqueSize, 1, MPI_INT, 0, MSG_FINISH_CLIQUE, MPI_COMM_WORLD);
}

void Communication::finish() {
	for (int dest = 1; dest < this->procCount; dest++) {
		MPI_Send (&this->color, 1, MPI_INT, dest, MSG_FINISH_CLIQUE, MPI_COMM_WORLD);
	}
	MPI_Status status;
	int tmpCliqueSize = 0;
	for (int source = 1; source < this->procCount; source++) {
    	MPI_Recv(&tmpCliqueSize, 1, MPI_INT, MPI_ANY_SOURCE, MSG_FINISH_CLIQUE, MPI_COMM_WORLD, &status);
		if (tmpCliqueSize > this->foundedCliqueSize) {
			this->foundedCliqueSize = tmpCliqueSize;
		}
	}
	this->sendFinishAll();
}

void Communication::sendToken() {
    int dest = (this->myIndex + 1) % this->procCount;

    if (color == BLACK) {
        token = BLACK;
	}
	if(myIndex == 0){
		 token = WHITE;
	}
#if DEBUG 
    cout << myIndex << ": Sending " << token << " token to " << dest << endl;
#endif
    MPI_Send(&token, 1, MPI_INT, dest, MSG_TOKEN, MPI_COMM_WORLD);
	token = NONE;
}

bool Communication::recvToken(int from) {
	MPI_Status status;
	MPI_Recv(&this->token, 1, MPI_INT, from, MSG_TOKEN, MPI_COMM_WORLD, &status);
#if DEBUG 
    cout << myIndex << ": Reciev " << token << " token from " << from << endl;
#endif
	if (this->myIndex == 0 && this->token == WHITE) {
		this->finish();
		return true;
	}
	return false;
}

void Communication::sendWork(string serializedStack) {
#if DEBUG 
	cout << myIndex << ": Sending work to " << this->lastRecvProccesserIndex << "." << endl;
#endif
	if (this->lastRecvProccesserIndex < this->myIndex ) {
		this->color = BLACK;
	}
    char *msg = strdup(serializedStack.c_str());
    MPI_Send(msg, serializedStack.length()+1, MPI_CHAR, this->lastRecvProccesserIndex, MSG_WORK_SENT, MPI_COMM_WORLD);
}

void Communication::sendNoWork() {
#if DEBUG 
    cout << myIndex << ": I have no work for " << this->lastRecvProccesserIndex << "." << endl;
#endif
    MPI_Send(&this->color, 1, MPI_INT, this->lastRecvProccesserIndex, MSG_WORK_NOWORK, MPI_COMM_WORLD);
}

string Communication::recvNoWork() {
#if DEBUG 
	cout << myIndex << ": I have recv no work." << endl;
#endif
	MPI_Status status;
	MPI_Recv(this->message, MESSAGE_LENGTH, MPI_CHAR, MPI_ANY_SOURCE, MSG_WORK_NOWORK, MPI_COMM_WORLD, &status);
	this->waitForWork=false;
	return "";
}

string Communication::recvWork() {
	MPI_Status status;
	MPI_Recv(this->message, MESSAGE_LENGTH, MPI_CHAR, MPI_ANY_SOURCE, MSG_WORK_SENT, MPI_COMM_WORLD, &status);
	this->waitForWork=false;
#if DEBUG 
	cout << myIndex << ": I have recv work." << endl;
#endif
	return this->message;
}

string Communication::askForWork(int maxFoundedCliqueSize) {
	int i=0;
	while(1) {
		if (!this->waitForWork){
			if (this->token != NONE) {
				sendToken();
				this->color = WHITE;
			}	
			i++;
#if DEBUG 
			cout << myIndex << ": Asking " << requestDest << " for work." << endl;
#endif
			MPI_Send(&this->color, 1, MPI_INT, this->requestDest, MSG_WORK_REQUEST, MPI_COMM_WORLD);

			this->requestDest = (this->requestDest + 1) % this->procCount;
			if (this->requestDest == this->myIndex) {
				this->requestDest = (this->requestDest + 1) % this->procCount;
			}
			this->waitForWork = true;
		}
		int msg = this->didIRecievedSomething();
		if (MSG_WORK_SENT == msg) {
			return this->message;
		} else if (MSG_FINISH == msg) {
				return "";
		} else if (MSG_WORK_REQUEST == msg) {
			this->sendNoWork();
		} else if (MSG_FINISH_CLIQUE == msg) {
			this->sendCliqueSize(maxFoundedCliqueSize);
		}
	}
}
