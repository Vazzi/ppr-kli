#include "graph.h"

#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <iostream>
#include "constants.h"
#include "serialization.h"

using namespace std;

Graph::Graph() {
    this->verticesCount = 0;
    this->vertexIndexes = NULL;
}

Graph::~Graph() {
    this->vertices.clear();
    delete this->vertexIndexes;
}

//---------------PUBLIC-------------------
string Graph::serialize() {
	string ret = "";
	for (vector<Vertex>::iterator it = this->vertices.begin();
            it != this->vertices.end();
            ++it) {
		ret += VERTICES_DELIMITER + it->serialize() + VERTICES_DELIMITER;
	}
	return ret;
}

void Graph::unserialize(const string line) {
	unsigned int i = 0;
	int verticesCount = 0;
	string indexStr, matrixLine;

	while (i < line.length()) {
		if (line[i] == VERTICES_DELIMITER) {
			verticesCount++;
			i++;
            indexStr = Serialization::getStringToChar(VERTEX_INCIDENCE_DELIMITER, &i, line);
			int index = atoi(indexStr.c_str());
			i++;
			matrixLine = Serialization::getStringToChar(VERTICES_DELIMITER, &i, line); 
			this->vertices.push_back(Vertex(index, matrixLine));
		}

		i++;
	}
	this->vertexIndexes = new int[verticesCount];
	int newIndex = 0;
    for (vector<Vertex>::iterator it = this->vertices.begin();
            it != this->vertices.end();
            ++it) {
        this->vertexIndexes[it->index] = newIndex;
        newIndex++; 
    }
	this->verticesCount = verticesCount;
}

void Graph::loadFromFile(const char *path) {
    ifstream file(path, ios::in);
    string line;

    if (!file.is_open()) {
        throw 2;
    }

    int i = -1;
    while (getline(file, line)) {

        if (i == -1) { // First line
            // Save the number of verticies
            this->verticesCount = atoi(line.c_str());
            this->vertexIndexes = new int[this->verticesCount];

            for (int index = 0; index < this->verticesCount; index++) {
                this->vertexIndexes[index] = i;
            }
        } else { // Matrix lines
            this->vertices.push_back(Vertex(i, line));
        }
        i++;
    }

    file.close();
}

void Graph::sortVerticesByDegree() {
    // Descending sort
    // Sort vertices
    stable_sort(this->vertices.rbegin(), this->vertices.rend());
    // Reorder indexes by new vertices order
    int newIndex = 0;
    for (vector<Vertex>::iterator it = this->vertices.begin();
            it != this->vertices.end();
            ++it) {
        this->vertexIndexes[it->index] = newIndex;
        newIndex++; 
    }
}

int Graph::size() const {
    return this->verticesCount;
}

Vertex *Graph::getVertexByPosition(const int index) {
    // If index is out of bound return NULL
    if (index < 0 || index >= this->size()) {
        return NULL;
    }
    Vertex *vertexPointer = &(this->vertices[index]);
    return vertexPointer;
}

Vertex *Graph::getVertex(const int index) {
    // If index is out of bound return NULL
    if (index < 0 || index >= this->size()) {
        return NULL;
    }
    int currentIndex = this->vertexIndexes[index];
    Vertex *vertexPointer = &(this->vertices[currentIndex]);
    return vertexPointer;
}

vector<Vertex *> Graph::getVertices() {
    vector<Vertex *> vertices = vector<Vertex *>();
    for (int i = 0; i < this->size(); i++) {
        vertices.push_back(&(this->vertices[i]));
    }
    return vertices;
}

ostream & operator<< (ostream &output, const Graph &graph) {
    for (unsigned int i = 0; i < graph.vertices.size(); i++) {
        output << graph.vertices[i] << endl;
    }
    return output; 
}

