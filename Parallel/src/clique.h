#ifndef _CLIQUE_
#define _CLIQUE_

#include <string>
#include <vector>

#include "vertex.h"

class Clique {

public:

    Clique();
    Clique(Clique *clique);
    ~Clique();

    int size() const;
    void addVertex(Vertex *newVertex);
    Vertex *getVertex(const int index) const;
    bool isInClique(const Vertex *vertex) const;
    bool doVertexBelongToClique(Vertex *vertex);
    void removeVertex(Vertex *vertex);
    std::string serialize() const;

    Clique & operator= (const Clique &clique);

    friend std::ostream & operator<< (std::ostream &output, const Clique &clique);

protected:
    std::vector<Vertex *> vertices;
};

#endif

