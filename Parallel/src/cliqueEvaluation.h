#ifndef _CLIQUE_EVALUATION_
#define _CLIQUE_EVALUATION_

#include <vector>
#include <string>

#include "graph.h"
#include "clique.h"
#include "vertexStack.h"
#include "vertexStackItem.h"
#include "communication.h"

class CliqueEvaluation {

public:
    CliqueEvaluation(Communication *communication);
    ~CliqueEvaluation();

    int foundedCliqueSize();
    void addGraph(Graph *graph);
    std::string serialize();
    Graph *unserialize(const std::string line);
    void computeMaxPossibleCliqueSize();
    void findMaxClique();
    int getMaxPossibleCliqueSize();
    void initStackWithVerticesByPRank(const int rank, const int p);

private:
    Graph *graph;
    VertexStack stack;
    int maxPossibleCliqueSize;
    int maxFoundedCliqueSize;
    Communication *communication;

    void sendHalfStackToProccesser();
    void cliqueFoundedEndProccess();
    bool checkNeighboursOf(VertexStackItem *stackItem);
    void newMaxFoundedCliqueSize(int size);
    std::string serializeHalfStack();
    void unserializeStack(const std::string line);
    void clearStack();
    bool handleRecevedMessages();
    VertexStackItem *createVertexStackItem(Vertex *vertex, VertexStackItem * fromStackItem);
    
};

#endif

