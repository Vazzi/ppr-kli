#ifndef _VERTEX_STACK_ITEM_
#define _VERTEX_STACK_ITEM_

#include "vertex.h"
#include "clique.h"

class VertexStackItem {

public:
    int neighboursChecked;
    bool *checkedIndexes;
    Clique clique;
    Vertex *vertex;

    VertexStackItem(Vertex *vertex, const int verticesCount);
    ~VertexStackItem();

    void setCheckedIndexes(const bool *copyCheckedIndexes); 
private:
    int verticesCount;

    void initCheckedIndexes();
};

#endif
