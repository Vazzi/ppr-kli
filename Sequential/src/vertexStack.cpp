#include "vertexStack.h"

using namespace std;

VertexStack::VertexStack() {

}

VertexStack::~VertexStack() {
    // Empty
}

//---------------PUBLIC-------------------

void VertexStack::push(VertexStackItem *vertex) {
    this->vertices.push(vertex);
}

VertexStackItem *VertexStack::top() {
    VertexStackItem *lastVertex = this->vertices.top();
    return lastVertex;
}

void VertexStack::pop() {
    this->vertices.top() = NULL;
    this->vertices.pop();
}

bool VertexStack::isEmpty() const {
    return this->vertices.empty();
}

