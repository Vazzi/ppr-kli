#include "cliqueEvaluation.h"

#include "vertex.h"
#include <iostream>

using namespace std;

// Odkomentovat pokud je potřeba debug 
// #define DEBUG 1 

CliqueEvaluation::CliqueEvaluation(Graph *graph) {
    this->graph = graph;
    this->stack = VertexStack();
    this->currentVertexCheckIndex = 0;
    this->maxFoundedCliqueSize = 0;
    this->maxPossibleCliqueSize = 0;
}

CliqueEvaluation::~CliqueEvaluation() {
    this->graph = NULL;
}

//---------------PUBLIC-------------------
int CliqueEvaluation::foundedCliqueSize() {
    return this->maxFoundedCliqueSize;
}

int CliqueEvaluation::getMaxPossibleCliqueSize() {
   if (this->maxPossibleCliqueSize == 0) {
        this->computeMaxPossibleCliqueSize();
   }
   return this->maxPossibleCliqueSize;
}

void CliqueEvaluation::computeMaxPossibleCliqueSize() {
    int maxSize = this->graph->getVertex(0)->degree + 1;
    int verticesCount = 0;

    for (int i = 0; i < this->graph->size(); i++) {
        ++verticesCount;
        Vertex *vertex = this->graph->getVertexByPosition(i);
        if (vertex->degree < maxSize) {
            maxSize = vertex->degree + 1;
        }
        if (verticesCount >= maxSize) {
            break;
        }
    }

    this->maxPossibleCliqueSize = maxSize;
}

void CliqueEvaluation::findMaxClique() {
    this->prepareEvaluation();

    for (int i = 0; i < this->graph->size(); i++) {
        this->currentVertexCheckIndex = i;

        Vertex *vertex = this->graph->getVertexByPosition(i);

        if (this->maxPossibleCliqueSize > vertex->degree + 1) {
            this->maxPossibleCliqueSize = vertex->degree + 1;
        }
#if DEBUG
        cout << "---------" <<  i << "(" << vertex->index << ")" << "---------" << endl; 
        cout << "Max possible clique size: " << this->maxPossibleCliqueSize << endl;
#endif
        if (this->findCliqueForVertex(vertex)) {
            break;
        }

#if DEBUG
        cout << "Max founded clique size: " << this->maxFoundedCliqueSize << endl;
#endif
    }
    this->clearStack(); 

#if DEBUG
    cout << "------------------------" << endl; 
#endif
}

//---------------PRIVATE-------------------
bool CliqueEvaluation::findCliqueForVertex(Vertex *vertex) {
    // Init stack with first vertex
    VertexStackItem *stackItem = new VertexStackItem(vertex, this->graph->size());
    stackItem->clique.addVertex(vertex);
    this->stack.push(stackItem);

    while (!this->stack.isEmpty()) {
        VertexStackItem *nextVertex = this->stack.top();

        this->newMaxFoundedCliqueSize(nextVertex->clique.size());

        if (this->maxFoundedCliqueSize >= this->maxPossibleCliqueSize) {
            return true;
        }

        if (!this->checkNeighboursOf(nextVertex)) {
            this->stack.pop();
        }    

    }

    return false;
}

bool CliqueEvaluation::checkNeighboursOf(VertexStackItem *stackItem) {
    for (int i = stackItem->neighboursChecked; i < stackItem->vertex->degree; i++) {
        int neighbourIndex = stackItem->vertex->neighboursIndexes[i];

        if (stackItem->checkedIndexes[neighbourIndex] == true ||
          this->currentVertexCheckIndex > neighbourIndex ) {
            continue;
        }

        stackItem->checkedIndexes[neighbourIndex] = true;

        Vertex *neighbour = this->graph->getVertex(neighbourIndex);

        if (neighbour->degree < stackItem->clique.size()) {
            continue;
        }

        if (stackItem->clique.isInClique(neighbour)) {
            continue;
        }

        // Neighbour vertex belong to clique?
        if (stackItem->clique.doVertexBelongToClique(neighbour)) {
            // Update next neighbour index to check 
            stackItem->neighboursChecked = i + 1;
            // Add neighbour vertex to stack
            this->stack.push(this->createVertexStackItem(neighbour, stackItem)); 
            return true;
        }
    }

    return false;
}

void CliqueEvaluation::prepareEvaluation() {
    if (this->maxPossibleCliqueSize == 0) {
        this->computeMaxPossibleCliqueSize();
    }

    this->maxFoundedCliqueSize = 0;
}

void CliqueEvaluation::newMaxFoundedCliqueSize(int const size) {
    if (size > this->maxFoundedCliqueSize) {
        this->maxFoundedCliqueSize = size;
    }
}

void CliqueEvaluation::clearStack() {
    while(!this->stack.isEmpty()) {
        this->stack.pop();
    }
}

VertexStackItem *CliqueEvaluation::createVertexStackItem(Vertex *vertex, VertexStackItem * fromStackItem) {
    VertexStackItem *newStackItem = new VertexStackItem(vertex, this->graph->size());
    newStackItem->setCheckedIndexes(fromStackItem->checkedIndexes);
    newStackItem->clique = fromStackItem->clique;
    newStackItem->clique.addVertex(vertex);

    return newStackItem;
}
