#ifndef _VERTEX_STACK_
#define _VERTEX_STACK_

#include <stack>

#include "vertexStackItem.h"
#include "graph.h"

class VertexStack {

public:
    VertexStack();
    ~VertexStack();

    void push(VertexStackItem *vertex);
    VertexStackItem *top();
    void pop();
    bool isEmpty() const;

protected:
    std::stack<VertexStackItem *> vertices;
};

#endif

