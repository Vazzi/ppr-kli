#include <iostream>
#include "mpi.h"
#include "graph.h"
#include "cliqueEvaluation.h"

using namespace std;

int main(int argc, char* argv[]) {
    double t1, t2;

    MPI_Init( &argc, &argv );

    t1=MPI_Wtime();

	cout << "Loading graph..." << endl;

    Graph graph = Graph();
    try {
        graph.loadFromFile(argv[1]);
    } catch (int e) {
        cout << "File loading error." << endl;
        return 1;
    }
    cout << "Graph loaded." << endl;

	cout << "Sorting graph..." << endl;
    graph.sortVerticesByDegree();
    cout << "Graph sorted." << endl;

    CliqueEvaluation cliqueEval = CliqueEvaluation(&graph);

    cout << "Max possible clique: " << cliqueEval.getMaxPossibleCliqueSize() << endl;

    cout << "Finding max. clique..." << endl;
    cliqueEval.findMaxClique();
    cout << "Maximum founded clique size: " << cliqueEval.foundedCliqueSize() << endl;

    t2=MPI_Wtime();

    printf("Elapsed time is %f.\n", t2-t1);

    MPI_Finalize();
    
    return 0;
}
