#include "vertexStackItem.h"

using std::vector;

VertexStackItem::VertexStackItem(Vertex *vertex, const int verticesCount) {
    this->vertex = vertex;
    this->clique = Clique();
    this->neighboursChecked = 0;
    this->verticesCount = verticesCount;
    this->initCheckedIndexes();
}

VertexStackItem::~VertexStackItem() {
    this->vertex = NULL;
    delete this->checkedIndexes;
}

void VertexStackItem::initCheckedIndexes() {
    this->checkedIndexes = new bool[verticesCount];

    for (int i = 0; i < this->verticesCount; i++) {
        this->checkedIndexes[i] = false;
    } 
}

void VertexStackItem::setCheckedIndexes(const bool *copyCheckedIndexes) {
    for (int i = 0; i < this->verticesCount; i++) {
        this->checkedIndexes[i] = copyCheckedIndexes[i];
    }
}
