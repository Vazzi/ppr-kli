#ifndef _CLIQUE_EVALUATION_
#define _CLIQUE_EVALUATION_

#include <vector>

#include "graph.h"
#include "clique.h"
#include "vertexStack.h"
#include "vertexStackItem.h"

class CliqueEvaluation {

public:
    CliqueEvaluation(Graph *graph);
    ~CliqueEvaluation();

    int foundedCliqueSize();
    void computeMaxPossibleCliqueSize();
    void findMaxClique();
    int getMaxPossibleCliqueSize();

private:
    Graph *graph;
    VertexStack stack;
    int currentVertexCheckIndex;
    int maxFoundedCliqueSize;
    int maxPossibleCliqueSize;

    bool findCliqueForVertex(Vertex *vertex);
    bool checkNeighboursOf(VertexStackItem *stackItem);
    void prepareEvaluation();
    void newMaxFoundedCliqueSize(int size);
    void clearStack();
    VertexStackItem *createVertexStackItem(Vertex *vertex, VertexStackItem * fromStackItem);
    
};

#endif

